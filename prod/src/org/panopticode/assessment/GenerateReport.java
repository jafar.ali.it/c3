package org.panopticode.assessment;

import static java.lang.Integer.parseInt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GenerateReport {

    private static final Map<Integer, String> longMethodMsg = new TreeMap<Integer, String>() {
        {
            put(11, "<=10 - Good");
            put(26, ">10 - Watch Out");
            put(51, ">25 - Scary");
            put(101, ">50 - Danger");
            put(1000, ">100 - Blindly Delete It");
        }
    };
    private static final Map<Integer, String> longTestMethodMsg = new TreeMap<Integer, String>() {
        {
            put(11, "<=10 - Good");
            put(21, ">10 - Watch Out");
            put(31, ">20 - Scary");
            put(51, ">30 - Danger");
            put(1000, ">50 - Blindly Delete It");
        }
    };
    private static final Map<Integer, String> complexityMsg = new TreeMap<Integer, String>() {
        {
            put(6, "<=5 - Good");
            put(8, ">5 - Watch Out");
            put(11, ">7 - Scary");
            put(26, ">10 - Danger");
            put(1000, ">25 - Blindly Delete It");
        }
    };
    private static final String BASE_LOCATION = ".";
    private static final String RESULTS_DIR = "target";
    private final String resultsDir;

    public GenerateReport(String resultsDir) {

        this.resultsDir = resultsDir;
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 1 && args[0].equalsIgnoreCase("header"))
            writeHeader();
        else if (args.length == 1 && args[0].equalsIgnoreCase("footer"))
            writeFooter();
        else if (args.length == 2)
            generateReport(args[0], args[1], RESULTS_DIR);
        else if (args.length == 3)
            generateReport(args[0], args[1], args[2]);
        else {
            writeHeader();
            generateReport("Naresh Jain", BASE_LOCATION, RESULTS_DIR);
            writeFooter();
        }
    }

    private static void generateReport(String name, String path, String resultsDir) throws Exception {
        StringBuilder result = new StringBuilder();
        GenerateReport q = new GenerateReport(resultsDir);

        int codeCoveragePercentage;
        try {
            int missedCoverage = parseInt(q.fetchSpecificValue("jacoco.xml", "/report/counter[@type='INSTRUCTION']/@missed"));
            int coveredCoverage = parseInt(q.fetchSpecificValue("jacoco.xml", "/report/counter[@type='INSTRUCTION']/@covered"));
            codeCoveragePercentage = coveredCoverage * 100 / (missedCoverage + coveredCoverage);
        } catch (IOException e) {
            codeCoveragePercentage = 0;
        }

        int longestClass = q.fetchHighestValue("javancss.xml", "//objects/object/ncss/text()");
        int totalSingleLineComments = parseInt(q.fetchSpecificValue("javancss.xml", "//packages/total/single_comment_lines/text()"));
        int totalMultiLineComments = parseInt(q.fetchSpecificValue("javancss.xml", "//packages/total/multi_comment_lines/text()"));

        String numberOfProdClasses = q.fetchSpecificValue("javancss.xml", "//packages/total/classes/text()");
        String totalProdLoC = q.fetchSpecificValue("javancss.xml", "//packages/total/ncss/text()");
        int longestProdMethod = q.fetchHighestValue("javancss.xml", "//functions/function/ncss/text()");
        int prodCCN = q.fetchHighestValue("javancss.xml", "//functions/function/ccn/text()");

        int numberOfTestClasses = parseInt(q.fetchSpecificValue("javancss_test.xml", "//packages/total/classes/text()"));
        int totalTestLoC = parseInt(q.fetchSpecificValue("javancss_test.xml", "//packages/total/ncss/text()"));
        int totalTestMethod = parseInt(q.fetchSpecificValue("javancss_test.xml", "//packages/total/functions/text()"));
        int longestTestMethod = q.fetchHighestValue("javancss_test.xml", "//functions/function/ncss/text()");

        String unitTestingSkills = "Unit Test Quality";
        String xUnitKnowledge = "Knowledge of xUnit";

        if (codeCoveragePercentage < 2) {
            unitTestingSkills = "Zero Code Coverage.";
            xUnitKnowledge = "None";
        } else if (codeCoveragePercentage < 10)
            unitTestingSkills = "Very low coverage.";

        if (numberOfTestClasses == 1)
            if (totalTestLoC < 7)
                xUnitKnowledge = "None";
            else if (totalTestMethod == 1)
                xUnitKnowledge = "Poor";

        String codeSmells = "Code Smells";
        if (q.hasSmell("magic number"))
            codeSmells = "Magic Numbers";
        if (q.hasSmell("Unused import"))
            codeSmells += "<br>Dead Code";
        if (q.hasSmell("Found duplicate"))
            codeSmells += "<br>Duplicate Code";
        if (q.hasSmell("must be private and have accessor methods"))
            codeSmells += "<br>Indecent Exposure";
        if (longestClass > 60)
            codeSmells += "<br>Large Class";
        if (longestProdMethod > 10)
            codeSmells += "<br>Long Method";
        if (q.highestValueForError("Cyclomatic Complexity") > 6)
            codeSmells += "<br>Conditional Complexity";
        if (totalSingleLineComments > 1 || totalMultiLineComments > 1)
            codeSmells += "<br>Comments";

        String longestMethodMsg = msgLookUp(longestProdMethod, longMethodMsg);
        String longestTestMethodMsg = msgLookUp(longestTestMethod, longTestMethodMsg);

        int nPathComplexity = q.highestValueForError("NPath Complexity");
        int fanOutComplexity = q.highestValueForError("Fan-Out Complexity");

        String nPathComplexityMsg = msgLookUp(nPathComplexity, complexityMsg);
        String fanOutComplexityMsg = msgLookUp(fanOutComplexity, complexityMsg);

        result.append("\n<tr>\n<td>").append(name).append("</td>\n");
        result.append("<td>?</td>\n");
        result.append("<td>Pre/Post</td>\n");
        result.append("<td>Overall Score</td>\n");
        result.append("<td>Understood the Problem</td>\n");
        result.append("<td>Program Meets Requirements</td>\n");
        result.append("<td>Overall</td>\n");
        result.append("<td>Communicative</td>\n");
        result.append("<td>Simple</td>\n");
        result.append("<td>Flexible</td>\n");
        result.append("<td>").append(numberOfProdClasses).append("</td>\n");
        result.append("<td>").append(totalProdLoC).append("</td>\n");
        result.append("<td>").append(codeSmells).append("</td>\n");
        result.append("<td>").append(longestMethodMsg).append("</td>\n");
        result.append("<td class='map_image'>").append(prodCCN).append(" <img src='").append(resultsDir).append("/reports/png/complexity-treemap.svg.png' alt='Cyclomatic Complexity'/></td>\n");
        result.append("<td>").append(nPathComplexityMsg).append("</td>\n");
        result.append("<td>").append(fanOutComplexityMsg).append("</td>\n");

        result.append("<td>").append(xUnitKnowledge).append("</td>\n");
        result.append("<td>").append(unitTestingSkills).append("</td>\n");
        result.append("<td>").append(numberOfTestClasses).append("</td>\n");
        result.append("<td>").append(totalTestLoC).append("</td>\n");
        result.append("<td>").append(longestTestMethodMsg).append("</td>\n");
        result.append("<td class='map_image'>").append(codeCoveragePercentage).append("% <img src='").append(resultsDir).append("/reports/png/coverage-treemap.svg.png' alt='Code Coverage'/></td>\n");
        result.append("</tr>");

        writeFile(result.toString(), path, true);
    }

    private static String msgLookUp(int count, Map<Integer, String> mapping) {
        for (int threshold : mapping.keySet())
            if (count <= threshold)
                return count + " (" + mapping.get(threshold) + ")";
        return String.valueOf(count);
    }

    private static void writeFooter() throws IOException {
        writeFile("</table></body></html>", BASE_LOCATION, true);
    }

    private static void writeHeader() throws IOException {
        String result = "<html><head>" +
                "<title>Assessment Report</title>" +
                "<style>table td {text-align: center} " +
                "\n br {mso-data-placement:same-cell;}" +
                "\n .map_image {width: 255px; height: 200px; vertical-align: top; } </style>" +
                "</head><body><table border='1'>\n" +
                "<tr>\n" +
                "<th>Name</th>\n" +
                "<th>Verdict</th>\n" +
                "<th>Assessment</th>\n" +
                "<th>Overall Score</th>\n" +
                "<th colspan='2' scope='colgroup'>Problem Solving</th>\n" +
                "<th colspan='4' scope='colgroup'>Design</th>\n" +
                "<th colspan='7' scope='colgroup'>Coding</th>\n" +
                "<th colspan='6' scope='colgroup'>Testing</th>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<th></th>\n" +
                "<th></th>\n" +
                "<th></th>\n" +
                "<th></th>\n" +
                "<th>Understood the core problem</th>\n" +
                "<th>Program meets requirements</th>\n" +
                "<th>Overall</th>\n" +
                "<th>Communicative</th>\n" +
                "<th>Simple</th>\n" +
                "<th>Flexible</th>\n" +
                "<th># of Classes</th>\n" +
                "<th>Total Code Size</th>\n" +
                "<th>Code Smells in the code</th>\n" +
                "<th>Longest Method</th>\n" +
                "<th>Cyclomatic Complexity</th>\n" +
                "<th>NPath Complexity</th>\n" +
                "<th>Fan-Out Complexity</th>\n" +
                "<th>Knowledge of xUnit</th>\n" +
                "<th>Quality of Unit Test</th>\n" +
                "<th># of Tests</th>\n" +
                "<th>Total Code Size</th>\n" +
                "<th>Longest Test Method</th>\n" +
                "<th>Test Coverage</th>\n" +
                "</tr>\n";
        writeFile(result, BASE_LOCATION, false);
    }

    private static void writeFile(String content, String location, boolean append) throws IOException {
        File file = new File(location + "/" + "AssessmentReport.html");
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, append)));
        out.println(content);
        out.flush();
        out.close();
    }

    private boolean exists(String fileName, String xPath) throws Exception {
        Document doc = getDocument(fileName);
        XPathExpression expr = getXPathExpression(xPath);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        return ((NodeList) result).getLength() > 0;
    }

    private int fetchHighestValue(String fileName, String xPath) throws Exception {
        Document doc = getDocument(fileName);
        XPathExpression expr = getXPathExpression(xPath);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        int highestValue = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            String value = nodes.item(i).getNodeValue();
            int currentValue = extractFirstDigit(value);
            if (currentValue > highestValue)
                highestValue = currentValue;
        }
        return highestValue;
    }

    private static int extractFirstDigit(String value) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(value);
        int currentValue = 0;
        if (m.find(0))
            currentValue = Integer.parseInt(m.group(0));
        return currentValue;
    }

    private String fetchSpecificValue(String fileName, String xPath) throws Exception {
        Document doc = getDocument(fileName);
        XPathExpression expr = getXPathExpression(xPath);
        Object result = expr.evaluate(doc, XPathConstants.NODE);
        return ((Node) result).getNodeValue();
    }

    private XPathExpression getXPathExpression(String xPath) throws XPathExpressionException {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        return xpath.compile(xPath);
    }

    private Document getDocument(String fileName) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        return builder.parse(resultsDir + "/rawmetrics/xml/" + fileName);
    }

    private boolean hasSmell(String smell) throws Exception {
        return exists("checkstyle.xml", "//error[contains(@message, \"" + smell + "\")]/@message");
    }

    private int highestValueForError(String error) throws Exception {
        return fetchHighestValue("checkstyle.xml", "//error[contains(@message, \"" + error + "\")]/@message");
    }
}
