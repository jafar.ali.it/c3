package org.panopticode.supplement.ant;

import org.panopticode.PanopticodeProject;
import org.panopticode.Supplement;
import org.panopticode.supplement.jacoco.JacocoSupplement;

import java.io.File;

public class JacocoPanopticode extends SupplementTask {

	private File jacocoFile;

	public JacocoPanopticode() {
		super(new JacocoSupplement());
    }

	public JacocoPanopticode(Supplement supplement) {
		super(supplement);
    }

	public void setJacocoFile(File file) { this.jacocoFile = file; }
	
	@Override protected void requireAttributes() {
	    requireAttribute(jacocoFile, "jacocoFile");
        requireFileExists(jacocoFile, "jacocoFile");
    }

	@Override protected void loadSupplement(Supplement supplement, PanopticodeProject project) {
	    supplement.loadData(project, new String[] {jacocoFile.getPath()});
    }
}
