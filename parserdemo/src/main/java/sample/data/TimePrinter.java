package sample.data;

import java.time.LocalDateTime;
import java.util.function.Predicate;

public class TimePrinter {
    public static void main(String[] args) {
        System.out.println(LocalDateTime.now());
    }

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    public static String toStr(int a) {
        switch (a) {
            case 0:
                return "ZERO";
            case 1:
                return "ONE";

            default:
                return "NA";
        }
    }

    public static Predicate<Integer> isEven = n -> n % 2 == 0;
}
