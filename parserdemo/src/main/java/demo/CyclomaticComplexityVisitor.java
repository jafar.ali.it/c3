package demo;

import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class CyclomaticComplexityVisitor extends VoidVisitorAdapter<Object> {

    private int complexity = 1;

    private void upComplexity() {
        complexity++;
    }

    @Override
    public void visit(IfStmt n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    @Override
    public void visit(ConditionalExpr n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    @Override
    public void visit(SwitchStmt n, Object arg) {
        super.visit(n, arg);
    }

    @Override
    public void visit(SwitchEntryStmt n, Object arg) {
        if (!isDefaultCase(n))
            upComplexity();
        super.visit(n, arg);
    }

    private static boolean isDefaultCase(SwitchEntryStmt n) {
        return n.toString().contains("default");
    }

    @Override
    public void visit(TryStmt n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    @Override
    public void visit(WhileStmt n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    @Override
    public void visit(DoStmt n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    @Override
    public void visit(ForEachStmt n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    @Override
    public void visit(ForStmt n, Object arg) {
        upComplexity();
        super.visit(n, arg);
    }

    public int getComplexity() {
        return complexity;
    }

}
