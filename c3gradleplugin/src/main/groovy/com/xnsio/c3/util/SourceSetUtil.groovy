package com.xnsio.c3.util

import org.gradle.api.Project

class SourceSetUtil {

    def static getJavaSourceSet(Project project) {
        if(isAndroidProject(project)){
            project.android.sourceSets.main.java.srcDirs[0]
        }
        else {
            project.sourceSets.main.allJava.srcDirs[0]
        }
    }

    private static boolean isAndroidProject(Project project) {
        return project.c3.projectType == 'android'
    }

    def static getJavaSourceSetsAsPath(Project project) {
        if(isAndroidProject(project)){
            project.android.sourceSets.main.java.srcDirs.join(System.getProperty('path.separator'))
        }
        else {
            project.sourceSets.main.allJava.srcDirs.join(System.getProperty('path.separator'))
        }
    }
}
