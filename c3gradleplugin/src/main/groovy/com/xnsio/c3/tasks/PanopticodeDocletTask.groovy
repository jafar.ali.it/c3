package com.xnsio.c3.tasks

import com.xnsio.c3.util.SourceSetUtil
import org.gradle.api.tasks.Exec

class PanopticodeDocletTask extends Exec {
    File c3Dir
    def panopticodePath
    def userClasspath
    String command

    PanopticodeDocletTask() {
        project.logger.warn("C3 : " + project.c3.c3Directory)
        c3Dir = new File("${project.buildDir}/${project.c3.c3Directory}")
        c3Dir.mkdirs()
        panopticodePath = project.buildscript.configurations.classpath.asPath
        userClasspath = project.sourceSets.main.compileClasspath.asPath
        project.logger.info("Panopticode classpath: " + panopticodePath)
        command = "javadoc -docletpath ${panopticodePath} -doclet org.panopticode.doclet.PanopticodeDoclet -private -subpackages . -projectName ${project.name} -debug true -outputFile ${c3Dir}/panopticode.xml -sourcepath ${SourceSetUtil.getJavaSourceSetsAsPath(project)} -classpath ${userClasspath}".toString()
        commandLine command.tokenize()
    }
}