package com.xnsio.c3.tasks

import org.gradle.api.tasks.JavaExec

class C3ReportGeneratorTask extends JavaExec {
    C3ReportGeneratorTask() {
        project.logger.info("Generating C3 report")
        classpath = project.buildscript.configurations.classpath
        main = 'org.panopticode.ReportRunner'
        args "org.panopticode.report.treemap.C3Treemap ${project.buildDir}/${project.c3.c3Directory}/panopticode.xml ${project.buildDir}/${project.c3.c3Directory}/c3-interactive-treemap.svg -interactive".trim().tokenize()
    }
}
