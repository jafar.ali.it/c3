package com.xnsio.c3.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ImportJDependAnalysisTask extends DefaultTask  {

    File jDependXmlReportFile

    @TaskAction
    def importJDependAnalysisTask() {
        project.logger.info("Importing dependency data")
        project.logger.info("Enhancing Panopticode with JDepend report: {}", jDependXmlReportFile)

        ant.taskdef(name: 'jdepend',
            classname: 'org.panopticode.supplement.ant.JDependPanopticode',
            classpath: project.buildscript.configurations.classpath.asPath)

        ant.jdepend(
            panopticodeFile: "${project.buildDir}/${project.c3.c3Directory}/panopticode.xml",
            jdependFile: "${jDependXmlReportFile}"
        )
    }
}
