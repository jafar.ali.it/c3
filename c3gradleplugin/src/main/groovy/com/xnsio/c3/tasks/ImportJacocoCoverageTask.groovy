package com.xnsio.c3.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ImportJacocoCoverageTask extends DefaultTask {

    File jacocoXmlReportFile

    @TaskAction
    def importComplexity() {
        project.logger.info("Importing complexity data")
        ant.taskdef(name: 'jacoco',
                classname: 'org.panopticode.supplement.ant.JacocoPanopticode',
                classpath: project.buildscript.configurations.classpath.asPath)

        ant.jacoco(
                panopticodeFile: "${project.buildDir}/${project.c3.c3Directory}/panopticode.xml",
                jacocoFile: jacocoXmlReportFile
        )
    }
}
