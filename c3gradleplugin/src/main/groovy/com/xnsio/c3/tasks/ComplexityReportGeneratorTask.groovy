package com.xnsio.c3.tasks

import org.gradle.api.tasks.JavaExec

class ComplexityReportGeneratorTask extends JavaExec {
    ComplexityReportGeneratorTask() {
        project.logger.info("Generating Complexity report")
        classpath = project.buildscript.configurations.classpath
        main = 'org.panopticode.ReportRunner'
        args "org.panopticode.report.treemap.ComplexityTreemap ${project.buildDir}/${project.c3.c3Directory}/panopticode.xml ${project.buildDir}/${project.c3.c3Directory}/complexity-interactive-treemap.svg -interactive".trim().tokenize()
    }
}
