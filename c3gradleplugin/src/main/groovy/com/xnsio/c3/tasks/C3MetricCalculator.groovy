package com.xnsio.c3.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class C3MetricCalculator extends DefaultTask {

    @TaskAction
    def calculateC3() {
        project.logger.info ("Calculating C3")
        ant.taskdef(name: 'panopticode',
                classname: 'org.panopticode.supplement.ant.C3Panopticode',
                classpath: project.buildscript.configurations.classpath.asPath)

        ant.panopticode(
                panopticodeFile: "${project.buildDir}/${project.c3.c3Directory}/panopticode.xml"
        )
    }
}
