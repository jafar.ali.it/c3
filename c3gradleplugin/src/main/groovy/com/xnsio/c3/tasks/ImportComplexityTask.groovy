package com.xnsio.c3.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ImportComplexityTask extends DefaultTask {

    @TaskAction
    def importComplexity() {
        project.logger.info("Importing complexity data")
        ant.taskdef(name: 'complexity',
                classname: 'org.panopticode.supplement.ant.JavaNCSSPanopticode',
                classpath: project.buildscript.configurations.classpath.asPath)

        def c3Dir = new File("${project.buildDir}/${project.c3.c3Directory}")
        ant.complexity(
                panopticodeFile: "${c3Dir}/panopticode.xml",
                javancssFile: "${project.buildDir}/${project.c3.c3Directory}/javancss-report.xml"
        )
    }
}
