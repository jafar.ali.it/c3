/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.OrderedTreemap;
import edu.umd.cs.treemap.Rect;

public class PivotByMiddle
implements MapLayout {
    OrderedTreemap orderedTreemap = new OrderedTreemap();

    public PivotByMiddle() {
        this.orderedTreemap.setPivotType(1);
    }

    public void layout(MapModel model, Rect bounds) {
        this.orderedTreemap.layout(model, bounds);
    }

    public String getName() {
        return "Pivot by Mid. / Ben B.";
    }

    public String getDescription() {
        return "Pivot by Middle, with stopping conditions added by Ben Bederson.";
    }
}

