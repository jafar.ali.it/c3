/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.demo;

import edu.umd.cs.treemap.demo.BufferPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

class MovingGraph
extends BufferPanel {
    private double yMax = 100.0;
    private int resolution = 10;
    private int step = 0;
    private double max;
    private double min;

    MovingGraph() {
    }

    void clear() {
        this.clearBuffer();
        this.step = 0;
    }

    void setYMax(double yMax) {
        this.yMax = yMax;
    }

    protected void drawOffscreen(Graphics g) {
        int h = this.size().height;
        int w = this.size().width;
        g.copyArea(0, 0, w, h, -1, 0);
        g.setColor(Color.black);
        int y1 = h - 1 - (int)((double)h * this.min / this.yMax);
        int y2 = h - 1 - (int)((double)h * this.max / this.yMax);
        g.setColor(Color.gray);
        g.drawLine(w - 1, h - 1, w - 1, y2);
        g.setColor(this.getBackground());
        g.drawLine(w - 1, y2 - 1, w - 1, 0);
    }

    synchronized void nextValue(double v) {
        if (++this.step < this.resolution) {
            if (this.step == 1) {
                this.max = v;
                this.min = v;
                return;
            }
            this.max = Math.max(v, this.max);
            this.min = Math.min(v, this.min);
            return;
        }
        this.step = 0;
        this.redraw();
    }
}

