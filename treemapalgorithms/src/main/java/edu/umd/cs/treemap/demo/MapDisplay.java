/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.demo;

import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import edu.umd.cs.treemap.demo.BufferPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;

class MapDisplay
extends BufferPanel {
    private Rect bounds;
    private Mappable[] items;
    private Color outlineColor = Color.black;
    private Color[] fillColor;
    private int thickness = 1;
    private int breadth;
    private boolean fillByOrder;

    MapDisplay(Rect bounds) {
        this.bounds = bounds;
        this.fillColor = new Color[256];
        for (int i = 0; i < 256; ++i) {
            this.fillColor[255 - i] = new Color(i, i, i);
        }
    }

    void setFillByOrder(boolean fillByOrder) {
        this.fillByOrder = fillByOrder;
    }

    void setItems(Mappable[] items, int breadth) {
        this.items = items;
        this.breadth = breadth;
    }

    void setRealBounds(Rect bounds) {
        this.bounds = bounds;
    }

    protected void drawOffscreen(Graphics g) {
        this.clearBuffer();
        if (this.items == null) {
            return;
        }
        int w = this.size().width - 1;
        int h = this.size().height - 1;
        for (int i = 0; i < this.items.length; ++i) {
            Mappable m = this.items[i];
            Rect r = m.getBounds();
            int x = (int)Math.round((double)w * r.x / this.bounds.w);
            int width = (int)Math.round((double)w * (r.x + r.w) / this.bounds.w) - x;
            int y = (int)Math.round((double)h * r.y / this.bounds.h);
            int height = (int)Math.round((double)h * (r.y + r.h) / this.bounds.h) - y;
            g.setColor(this.fillByOrder ? this.fillColor[128 * m.getOrder() / this.breadth] : Color.white);
            g.fillRect(x, y, width, height);
            g.setColor(this.outlineColor);
            for (int j = 0; j < this.thickness; ++j) {
                g.drawRect(x + j, y + j, width - 2 * j, height - 2 * j);
            }
        }
        Toolkit.getDefaultToolkit().sync();
    }
}

