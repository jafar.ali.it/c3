/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.test;

import edu.umd.cs.treemap.MapItem;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;

public class TestMap
implements MapModel {
    static double[] testSize = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.001, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    Mappable[] items;

    public TestMap() {
        this(testSize);
    }

    public TestMap(double[] size) {
        int n = size.length;
        this.items = new MapItem[n];
        for (int i = 0; i < n; ++i) {
            this.items[i] = new MapItem(size[i], i);
        }
    }

    public Mappable[] getItems() {
        return this.items;
    }
}

