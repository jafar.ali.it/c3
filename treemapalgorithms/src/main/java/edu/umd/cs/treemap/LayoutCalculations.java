/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import edu.umd.cs.treemap.TreeModel;
import java.io.PrintStream;

public class LayoutCalculations {
    public static double averageAspectRatio(MapModel model) {
        return LayoutCalculations.averageAspectRatio(model.getItems());
    }

    public static double averageAspectRatio(Mappable[] m) {
        double s = 0.0;
        int n = m.length;
        if (m == null || n == 0) {
            System.out.println("Can't measure aspect ratio.");
            return 0.0;
        }
        for (int i = 0; i < n; ++i) {
            s += m[i].getBounds().aspectRatio();
        }
        return s / (double)n;
    }

    public static double getReadability(TreeModel tree) {
        return LayoutCalculations.getReadability(tree.getLeafModels());
    }

    public static double getReadability(MapModel[] model) {
        int weight = 0;
        double r = 0.0;
        for (int i = 0; i < model.length; ++i) {
            int n = model[i].getItems().length;
            weight += n;
            r += (double)n * LayoutCalculations.getReadability(model[i]);
        }
        return weight == 0 ? 1.0 : r / (double)weight;
    }

    public static double getReadability(MapModel model) {
        int numTurns = 0;
        double prevAngle = 0.0;
        double angle = 0.0;
        double angleChange = 0.0;
        Mappable[] items = model.getItems();
        for (int i = 1; i < items.length; ++i) {
            Rect b1 = items[LayoutCalculations.getItemIndex(i - 1, model)].getBounds();
            Rect b2 = items[LayoutCalculations.getItemIndex(i, model)].getBounds();
            double dx = b2.x + 0.5 * b2.w - (b1.x + 0.5 * b1.w);
            double dy = b2.y + 0.5 * b2.h - (b1.y + 0.5 * b1.h);
            angle = Math.atan2(dy, dx);
            if (i >= 2) {
                angleChange = Math.abs(angle - prevAngle);
                if (angleChange > 3.141592653589793) {
                    angleChange = Math.abs(angleChange - 6.283185307179586);
                }
                if (angleChange > 0.1) {
                    ++numTurns;
                }
            }
            prevAngle = angle;
        }
        double readability = 1.0 - (double)numTurns / (double)items.length;
        return readability;
    }

    public static int getItemIndex(int order, MapModel map) {
        int i;
        Mappable[] items = map.getItems();
        for (i = 0; i < items.length && items[i].getOrder() != order; ++i) {
        }
        return i;
    }
}

