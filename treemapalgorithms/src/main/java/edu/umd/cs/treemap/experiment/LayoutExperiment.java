/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.experiment;

import edu.umd.cs.treemap.BinaryTreeLayout;
import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.PivotByMiddle;
import edu.umd.cs.treemap.PivotBySize;
import edu.umd.cs.treemap.PivotBySplitSize;
import edu.umd.cs.treemap.SliceLayout;
import edu.umd.cs.treemap.SquarifiedLayout;
import edu.umd.cs.treemap.StripTreemap;
import edu.umd.cs.treemap.experiment.Experiment;
import edu.umd.cs.treemap.experiment.Result;
import java.io.PrintStream;
import java.util.Date;

public class LayoutExperiment {
    static final int STEPS = 100;
    static final int TRIALS = 100;
    static MapLayout[] algorithms = new MapLayout[]{new SliceLayout(), new SquarifiedLayout(), new StripTreemap(), new BinaryTreeLayout(), new PivotByMiddle(), new PivotBySize(), new PivotBySplitSize()};

    public static void main(String[] args) {
        LayoutExperiment.performExperiments();
    }

    static void performExperiments() {
        System.out.println("===================");
        System.out.println("LayoutExperiment:");
        System.out.println("Started: " + new Date());
        LayoutExperiment.perform(100, 100, 20, 1, 1L, 0);
        LayoutExperiment.perform(100, 100, 20, 1, 1L, 1);
        LayoutExperiment.perform(100, 100, 100, 1, 1L, 0);
        LayoutExperiment.perform(100, 100, 100, 1, 1L, 1);
        LayoutExperiment.perform(100, 100, 8, 3, 1L, 0);
        LayoutExperiment.perform(100, 100, 8, 3, 1L, 1);
        System.out.println("==================");
        System.out.println("Finished: " + new Date());
    }

    static void perform(int steps, int trials, int breadth, int depth, long seed, int distribution) {
        System.out.println("===================");
        System.out.println((distribution == 0 ? "Gaussian" : "Zipf") + " distribution");
        System.err.println("---" + (distribution == 0 ? "Gaussian" : "Zipf") + " distribution");
        System.out.println("Tree breadth: " + breadth);
        System.err.println("---Tree breadth: " + breadth);
        System.out.println("Tree depth:   " + depth);
        System.err.println("---Tree depth:   " + depth);
        System.out.println("trials:       " + trials);
        System.out.println("steps:        " + steps);
        System.out.println("seed:         " + seed);
        System.out.println("--------------\n\n");
        for (int i = 0; i < algorithms.length; ++i) {
            MapLayout t = algorithms[i];
            System.err.println(t.getName());
            System.out.println(t.getName());
            Experiment experiment = new Experiment();
            Result result = experiment.run(t, steps, trials, breadth, depth, seed, distribution);
            System.out.println(result);
            System.out.println("\n........\n");
            try {
                Thread.sleep(60000L);
                continue;
            }
            catch (Exception e) {
                // empty catch block
            }
        }
    }
}

