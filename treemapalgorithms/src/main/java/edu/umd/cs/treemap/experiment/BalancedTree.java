/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.experiment;

import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.TreeModel;
import edu.umd.cs.treemap.experiment.ArrayBackedItem;

public class BalancedTree
extends TreeModel {
    private double[] array;

    public BalancedTree(int breadth, int depth) {
        this(BalancedTree.makeArray(breadth, depth), breadth, depth);
    }

    public static double[] makeArray(int breadth, int depth) {
        return new double[BalancedTree.countLeaves(breadth, depth)];
    }

    public BalancedTree(double[] array, int breadth, int depth) {
        this.array = array;
        int subSize = BalancedTree.countLeaves(breadth, depth - 1);
        for (int i = 0; i < breadth; ++i) {
            this.addChild(this.submodel(breadth, depth - 1, i * subSize, i));
        }
    }

    private TreeModel submodel(int breadth, int depth, int startIndex, int order) {
        if (depth == 0) {
            return new TreeModel(new ArrayBackedItem(this.array, startIndex, order));
        }
        TreeModel t = new TreeModel();
        int subSize = BalancedTree.countLeaves(breadth, depth - 1);
        for (int i = 0; i < breadth; ++i) {
            t.addChild(this.submodel(breadth, depth - 1, startIndex + i * subSize, i));
        }
        return t;
    }

    private static int countLeaves(int breadth, int depth) {
        return (int)Math.pow(breadth, depth);
    }

    public double[] getArray() {
        return this.array;
    }
}

