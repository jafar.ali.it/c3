/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.MapItem;
import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.MapModel;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import java.io.PrintStream;
import java.util.Vector;

public class TreeModel
implements MapModel {
    private Mappable mapItem;
    private Mappable[] childItems;
    private Mappable[] cachedTreeItems;
    private MapModel[] cachedLeafModels;
    private TreeModel parent;
    private Vector children = new Vector();
    private boolean sumsChildren;

    public TreeModel() {
        this.mapItem = new MapItem();
        this.sumsChildren = true;
    }

    public TreeModel(Mappable mapItem) {
        this.mapItem = mapItem;
    }

    public void setOrder(int order) {
        this.mapItem.setOrder(order);
    }

    public MapModel[] getLeafModels() {
        if (this.cachedLeafModels != null) {
            return this.cachedLeafModels;
        }
        Vector v = new Vector();
        this.addLeafModels(v);
        int n = v.size();
        MapModel[] m = new MapModel[n];
        v.copyInto(m);
        this.cachedLeafModels = m;
        return m;
    }

    private Vector addLeafModels(Vector v) {
        if (!this.hasChildren()) {
            System.err.println("Somehow tried to get child model for leaf!!!");
            return v;
        }
        if (!this.getChild(0).hasChildren()) {
            v.addElement(this);
        } else {
            for (int i = this.childCount() - 1; i >= 0; --i) {
                this.getChild(i).addLeafModels(v);
            }
        }
        return v;
    }

    public int depth() {
        if (this.parent == null) {
            return 0;
        }
        return 1 + this.parent.depth();
    }

    public void layout(MapLayout tiling) {
        this.layout(tiling, this.mapItem.getBounds());
    }

    public void layout(MapLayout tiling, Rect bounds) {
        this.mapItem.setBounds(bounds);
        if (!this.hasChildren()) {
            return;
        }
        double s = this.sum();
        tiling.layout(this, bounds);
        for (int i = this.childCount() - 1; i >= 0; --i) {
            this.getChild(i).layout(tiling);
        }
    }

    public Mappable[] getTreeItems() {
        if (this.cachedTreeItems != null) {
            return this.cachedTreeItems;
        }
        Vector v = new Vector();
        this.addTreeItems(v);
        int n = v.size();
        Mappable[] m = new Mappable[n];
        v.copyInto(m);
        this.cachedTreeItems = m;
        return m;
    }

    private void addTreeItems(Vector v) {
        if (!this.hasChildren()) {
            v.addElement(this.mapItem);
        } else {
            for (int i = this.childCount() - 1; i >= 0; --i) {
                this.getChild(i).addTreeItems(v);
            }
        }
    }

    private double sum() {
        if (!this.sumsChildren) {
            return this.mapItem.getSize();
        }
        double s = 0.0;
        for (int i = this.childCount() - 1; i >= 0; --i) {
            s += this.getChild(i).sum();
        }
        this.mapItem.setSize(s);
        return s;
    }

    public Mappable[] getItems() {
        if (this.childItems != null) {
            return this.childItems;
        }
        int n = this.childCount();
        this.childItems = new Mappable[n];
        for (int i = 0; i < n; ++i) {
            this.childItems[i] = this.getChild(i).getMapItem();
            this.childItems[i].setDepth(1 + this.depth());
        }
        return this.childItems;
    }

    public Mappable getMapItem() {
        return this.mapItem;
    }

    public void addChild(TreeModel child) {
        child.setParent(this);
        this.children.addElement(child);
        this.childItems = null;
    }

    public void setParent(TreeModel parent) {
        for (TreeModel p = parent; p != null; p = p.getParent()) {
            if (p != this) continue;
            throw new IllegalArgumentException("Circular ancestry!");
        }
        this.parent = parent;
    }

    public TreeModel getParent() {
        return this.parent;
    }

    public int childCount() {
        return this.children.size();
    }

    public TreeModel getChild(int n) {
        return (TreeModel)this.children.elementAt(n);
    }

    public boolean hasChildren() {
        return this.children.size() > 0;
    }

    public void print() {
        this.print("");
    }

    private void print(String prefix) {
        System.out.println(prefix + "size=" + this.mapItem.getSize());
        for (int i = 0; i < this.childCount(); ++i) {
            this.getChild(i).print(prefix + "..");
        }
    }
}

