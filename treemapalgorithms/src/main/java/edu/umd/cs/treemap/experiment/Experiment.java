/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap.experiment;

import edu.umd.cs.treemap.LayoutCalculations;
import edu.umd.cs.treemap.LayoutDifference;
import edu.umd.cs.treemap.MapLayout;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import edu.umd.cs.treemap.TreeModel;
import edu.umd.cs.treemap.experiment.BalancedTree;
import edu.umd.cs.treemap.experiment.Result;
import java.util.Random;

class Experiment {
    static final int GAUSSIAN_INITIAL = 0;
    static final int ZIPF_INITIAL = 1;
    private int steps;
    private int trials;
    private int breadth;
    private int distribution;
    private int depth;
    private long seed;
    private double change;
    private double aspect;
    private double[] sizes;
    private Rect bounds = new Rect(0.0, 0.0, 100.0, 100.0);

    Experiment() {
    }

    Result run(MapLayout t, int steps, int trials, int breadth, int depth, long seed, int distribution) {
        this.steps = steps;
        this.trials = trials;
        this.breadth = breadth;
        this.depth = depth;
        this.seed = seed;
        if (distribution < 0 || distribution >= 2) {
            throw new IllegalArgumentException("Unknown distribution: " + distribution);
        }
        this.distribution = distribution;
        Random random = new Random(seed);
        Result result = new Result();
        TreeModel tree = this.makeModel();
        for (int i = 0; i < trials; ++i) {
            Result r = this.runTrial(t, tree, random);
            result.aspect += r.aspect;
            result.change += r.change;
            result.readability += r.readability;
        }
        result.aspect /= (double)trials;
        result.change /= (double)trials;
        result.readability /= (double)trials;
        return result;
    }

    Result runTrial(MapLayout algorithm, TreeModel model, Random random) {
        for (int i = 0; i < this.sizes.length; ++i) {
            this.sizes[i] = this.distribution == 0 ? Math.exp(random.nextGaussian()) : 1.0 / (1.0 + (double)this.sizes.length * Math.random());
        }
        this.updateLayout(model, null, algorithm);
        Result result = new Result();
        for (int i = 0; i < this.steps; ++i) {
            this.updateData(random);
            this.updateLayout(model, result, algorithm);
        }
        result.aspect /= (double)this.steps;
        result.change /= (double)this.steps;
        result.readability /= (double)this.steps;
        return result;
    }

    TreeModel makeModel() {
        BalancedTree tree = new BalancedTree(this.breadth, this.depth);
        this.sizes = tree.getArray();
        return tree;
    }

    void updateData(Random random) {
        int i = 0;
        while (i < this.sizes.length) {
            double[] arrd = this.sizes;
            int n = i++;
            arrd[n] = arrd[n] * Math.exp(0.05 * random.nextGaussian());
        }
    }

    void updateLayout(TreeModel model, Result result, MapLayout algorithm) {
        LayoutDifference measure = new LayoutDifference();
        Mappable[] leaves = model.getTreeItems();
        measure.recordLayout(leaves);
        model.layout(algorithm, this.bounds);
        if (result == null) {
            return;
        }
        result.change += measure.averageDistance(leaves);
        result.aspect += LayoutCalculations.averageAspectRatio(leaves);
        result.readability += LayoutCalculations.getReadability(model);
    }
}

