/*
 * Decompiled with CFR 0.139.
 */
package edu.umd.cs.treemap;

import edu.umd.cs.treemap.AbstractMapLayout;
import edu.umd.cs.treemap.Mappable;
import edu.umd.cs.treemap.Rect;
import edu.umd.cs.treemap.SliceLayout;

public class SquarifiedLayout
extends AbstractMapLayout {
    public void layout(Mappable[] items, Rect bounds) {
        this.layout(this.sortDescending(items), 0, items.length - 1, bounds);
    }

    public void layout(Mappable[] items, int start, int end, Rect bounds) {
        double a;
        if (start > end) {
            return;
        }
        if (end - start < 2) {
            SliceLayout.layoutBest(items, start, end, bounds);
            return;
        }
        double x = bounds.x;
        double y = bounds.y;
        double w = bounds.w;
        double h = bounds.h;
        double total = this.sum(items, start, end);
        int mid = start;
        double b = a = items[start].getSize() / total;
        if (w < h) {
            while (mid <= end) {
                double aspect = this.normAspect(h, w, a, b);
                double q = items[mid].getSize() / total;
                if (this.normAspect(h, w, a, b + q) > aspect) break;
                ++mid;
                b += q;
            }
            SliceLayout.layoutBest(items, start, mid, new Rect(x, y, w, h * b));
            this.layout(items, mid + 1, end, new Rect(x, y + h * b, w, h * (1.0 - b)));
        } else {
            while (mid <= end) {
                double aspect = this.normAspect(w, h, a, b);
                double q = items[mid].getSize() / total;
                if (this.normAspect(w, h, a, b + q) > aspect) break;
                ++mid;
                b += q;
            }
            SliceLayout.layoutBest(items, start, mid, new Rect(x, y, w * b, h));
            this.layout(items, mid + 1, end, new Rect(x + w * b, y, w * (1.0 - b), h));
        }
    }

    private double aspect(double big, double small, double a, double b) {
        return big * b / (small * a / b);
    }

    private double normAspect(double big, double small, double a, double b) {
        double x = this.aspect(big, small, a, b);
        if (x < 1.0) {
            return 1.0 / x;
        }
        return x;
    }

    private double sum(Mappable[] items, int start, int end) {
        double s = 0.0;
        for (int i = start; i <= end; ++i) {
            s += items[i].getSize();
        }
        return s;
    }

    public String getName() {
        return "Squarified";
    }

    public String getDescription() {
        return "Algorithm used by J.J. van Wijk.";
    }
}

